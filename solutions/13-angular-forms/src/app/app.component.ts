import {Component} from '@angular/core';
import {User} from "./user.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    user: User = {name: 'Anna Conda', age: 42,};

    onSubmit(usr: User) {
        if (usr.name && usr.age) {
            console.log('[main] %o', usr);
            this.user = usr;
        }
    }
}
