import {Component} from '@angular/core';
import {UserService} from "./services/user.service";
import {User} from "./domain/user.model";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    users: User[] = [];
    user: User | undefined;
    editUser: User = {id: -1, name: '', age: 0};

    constructor(public userSvc: UserService) {
        userSvc.findAll().subscribe(objs => this.users = objs);
        //userSvc.findOne(222).subscribe(obj => this.user = obj);
    }

    show(id: number | undefined) {
        if (id) {
            this.userSvc.findOne(id).subscribe(obj => this.user = obj);
        }
    }

    clear() {
        this.user = undefined;
    }

    remove(id: number | undefined) {
        if (id) {
            this.userSvc.remove(id).subscribe(ok => {
                this.user = undefined;
                this.userSvc.findAll().subscribe(objs => this.users = objs);
            });
        }
    }

    save() {
        if (this.editUser.id === -1) { //create
            let payload = {name: this.editUser.name, age: this.editUser.age};
            this.userSvc.create(payload).subscribe(obj => {
                this.user = obj;
                this.editUser = {id: -1, name: '', age: 0};
                this.userSvc.findAll().subscribe(objs => this.users = objs);
            });
        } else { //update
            alert('update not yet implemented')
        }
    }
}
