import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../domain/user.model";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    readonly url: string = 'http://localhost:3000/users';

    constructor(private httpSvc: HttpClient) {
    }

    findAll(): Observable<User[]> {
        return this.httpSvc.get<User[]>(this.url);
    }

    findOne(id: number): Observable<User> {
        return this.httpSvc.get<User>(`${this.url}/${id}`);
    }

    remove(id: number): Observable<void> {
        return this.httpSvc.delete<void>(`${this.url}/${id}`);
    }

    create(user: User): Observable<User> {
        return this.httpSvc.post<User>(this.url, user);
    }

}
