import {readFileSync, writeFileSync} from 'node:fs';
import {Product} from './product';

const limit    = Number(process.argv[2] || 1200);
const outfile  = 'products-expensive.json';

const buffer   = readFileSync('./products.json');
const products = JSON.parse(buffer.toString())
                     .map((p: any) => Product.fromDB(p))
                     .filter((p: Product) => p.price >= limit)
                     .map((p: Product) => p.toDB());
writeFileSync(outfile, JSON.stringify(products, null, 3));
console.log('written %s (%d products)', outfile, products.length);
