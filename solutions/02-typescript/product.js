"use strict";
exports.__esModule = true;
exports.Product = void 0;
var Product = /** @class */ (function () {
    function Product(id, name, price, city) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.city = city;
    }
    Product.fromDB = function (data) {
        return new Product(data.id, data.name, data.price, data.city);
    };
    Product.prototype.toDB = function () {
        return Object.assign({}, this);
    };
    return Product;
}());
exports.Product = Product;
