"use strict";
exports.__esModule = true;
var node_fs_1 = require("node:fs");
var product_1 = require("./product");
var limit = Number(process.argv[2] || 1200);
var outfile = 'products-expensive.json';
var buffer = (0, node_fs_1.readFileSync)('./products.json');
var products = JSON.parse(buffer.toString())
    .map(function (p) { return product_1.Product.fromDB(p); })
    .filter(function (p) { return p.price >= limit; })
    .map(function (p) { return p.toDB(); });
(0, node_fs_1.writeFileSync)(outfile, JSON.stringify(products, null, 3));
console.log('written %s (%d products)', outfile, products.length);
