import {Component} from '@angular/core';
import {formatDate} from "@angular/common";

const ISO = 'yyyy-MM-dd';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    today: Date = new Date();
    offset: number = 5;

    get nextDate(): Date {
        const MS = 24 * 3600 * 1000;
        const ts = this.today.getTime() + this.offset * MS;
        return new Date(ts);
    }

    toISO(date: Date): string {
        return formatDate(date, ISO, 'en');
    }

    fromISO(date: string): Date {
        return new Date(date);
    }
}
