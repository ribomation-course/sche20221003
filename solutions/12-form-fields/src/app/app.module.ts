import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";

//https://stackoverflow.com/a/48409743
import {registerLocaleData} from "@angular/common";
import localeSv from '@angular/common/locales/sv';
registerLocaleData(localeSv);

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [
        { provide: LOCALE_ID, useValue: "sv-SE" }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
