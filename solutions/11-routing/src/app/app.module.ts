import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomePage} from './pages/home/home.page';
import {AboutPage} from './pages/about/about.page';
import {ProductsPage} from './pages/products/products.page';
import {NotFoundPage} from './pages/not-found/not-found.page';
import {RouterModule} from "@angular/router";
import {routes} from "./pages/routing-table";

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ProductsPage,
        NotFoundPage
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
