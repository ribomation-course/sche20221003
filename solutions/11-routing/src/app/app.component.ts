import {Component} from '@angular/core';
import {routes} from "./pages/routing-table";

interface NavItem {
    uri: string;
    text: string;
    visible?: boolean;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    readonly nav: NavItem[] = routes
        .filter(n => n.data ? n.data['nav'] : false)
        .map(n => ({
        uri: '/' + n.path,
        text: n.data ? n.data['text'] : '---',
        //visible: n.data ? n.data['nav'] : false
    }));

    readonly year = new Date().getFullYear();
    readonly org = 'Bugsify Ltd.';
}
