import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.css']
})
export class HomePage {
    imageUrl = 'https://live.staticflickr.com/4174/34609720326_06f2766601_b.jpg';

}
