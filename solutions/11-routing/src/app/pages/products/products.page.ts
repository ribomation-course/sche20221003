import {Component, OnInit} from '@angular/core';

interface Product {
    name: string;
    price: number;
}

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.css']
})
export class ProductsPage {
    products: Product[] = [
        {name: 'Apple', price: 1},
        {name: 'Banana', price: 2},
        {name: 'Coco Nut', price: 3},
        {name: 'Date Plum', price: 4},
    ];

}
