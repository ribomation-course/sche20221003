import {Route} from "@angular/router";
import {HomePage} from "./home/home.page";
import {AboutPage} from "./about/about.page";
import {ProductsPage} from "./products/products.page";
import {NotFoundPage} from "./not-found/not-found.page";

export const routes: Route[] = [
    {
        path: 'home', component: HomePage, data: {nav: true, text: 'Home Page'}
    },
    {
        path: 'about', component: AboutPage, data: {nav: true, text: 'About Page'}
    },
    {
        path: 'products', component: ProductsPage, data: {nav: true, text: 'Products Page'}
    },
    {
        path: '', redirectTo: '/home', pathMatch: 'full', data: {nav: false}
    },
    {
        path: '**', component: NotFoundPage, data: {nav: false}
    },
];
