import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
        <h1>User</h1>
        <p><strong>Name:</strong> [{{user?.name?.first}} {{user?.name?.last}}] </p>
        <button (click)="toggle()">Update User</button>
    `,
    styles: ['']
})
export class AppComponent {
    user: any = {
        name: {
            first: 'Anna', last: 'Conda'
        }
    }

    toggle() {
        if (this.user) {
            this.user = undefined;
        } else {
            this.user = {
                name: {
                    first: 'Per', last: 'Silja'
                }
            }
        }
    }
}
