import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Property, PropertyType} from "./property";

@Component({template: ''})
export class AbstractEditable<T extends PropertyType> implements OnInit {
    editing: boolean = false;
    editableValue: T | undefined;

    @Input('value') propertyValue: T | undefined;
    @Input('name') propertyName: string | undefined;
    @Input('hint') hint: string = 'Click to edit';
    @Output('updated') updatedEmitter = new EventEmitter<Property>();

    ngOnInit(): void {
        if (!this.propertyName) {
            throw new Error('[editable-text] missing property name')
        }
        if (!this.propertyValue) {
            throw new Error('[editable-text] missing property va;ue')
        }
    }

    get empty(): boolean {
        const val = this.propertyValue;
        if (!val) return true;
        if (val instanceof String) {
            return val.toString().trim().length === 0;
        }
        if (val instanceof Number) {
            return val.toString().trim().length === 0;
        }
        return false;
    }

    onKeyUp(evt: any) {
        if (evt.key === 'Escape') {
            this.revert();
        } else if (evt.key === 'Enter') {
            this.save();
        }
    }

    edit() {
        this.editableValue = this.propertyValue;
        this.editing = true;
    }

    revert() {
        this.editing = false;
        this.editableValue = undefined;
    }

    save() {
        this.editing = false;
        this.propertyValue = this.editableValue;
        this.editableValue = undefined;
        const payload: Property = {
            name: this.propertyName as string,
            value: this.propertyValue as string,
        };
        this.updatedEmitter.emit(payload);
    }

}
