import {Component} from '@angular/core';
import {Product} from "./domain/product";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    products: Product[] = [
        {name: 'Apple Fruit', price: 1234},
        {name: 'Angular Book', price: 5678},
        {name: 'Laptop Computer', price: 99999},
    ];

    result: any;

    onSaved(payload: any) {
        this.result = payload;
    }
}
