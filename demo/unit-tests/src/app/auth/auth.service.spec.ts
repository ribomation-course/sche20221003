import {AuthService} from './auth.service';

describe('AuthService', () => {
  let svc: AuthService;

  beforeEach(() => {
    svc = new AuthService();
  });

  afterEach(() => {
    svc = undefined;
    sessionStorage.removeItem(AuthService.TOKEN_KEY);
  });

  it('should not be authenticated from start', () => {
    expect(svc.isAuthenticated()).toBeFalsy();
  });

  it('login should return false if usr/pwd missing', () => {
    let rc = svc.login(null, null);
    expect(rc).toBeFalsy();
  });

  it('should be authenticated after login ', () => {
    expect(svc.isAuthenticated()).toBeFalsy();

    let rc = svc.login('anna.conda', 'secret');
    expect(rc).toBeTruthy();
    expect(svc.isAuthenticated()).toBeTruthy();
    expect(svc.getUsername()).toContain('conda');
  });

  it('should not be authenticated after logout', () => {
    let rc = svc.login('anna.conda', 'secret');
    expect(svc.isAuthenticated()).toBeTruthy();

    svc.logout();
    expect(svc.isAuthenticated()).toBeFalsy();
    expect(svc.getUsername()).toBeUndefined();
  });

});


