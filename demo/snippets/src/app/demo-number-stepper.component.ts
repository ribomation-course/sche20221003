import {Component} from "@angular/core";

@Component({
  selector: "app-demo-number-stepper",
  template: `
    <div>
      <h3>Two 1-way bindings</h3>
      <number-stepper [value]="value1" (valueChange)="value1 = $event"></number-stepper>
      Current value = {{value1}}
    </div>
    <div>
      <h3>One 2-way binding</h3>
      <number-stepper [(value)]="value2"></number-stepper>
      Current value = {{value2}}
    </div>
  `,
  styles: []
})
export class DemoNumberStepperComponent {
  value1: number = 22;
  value2: number = 42;
}
