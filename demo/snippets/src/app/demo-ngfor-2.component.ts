import { Component } from '@angular/core';

interface Person {
  name: string;
  age: number;
  male: boolean;
}
const age = () => 20 + Math.floor(40*Math.random());

@Component({
  selector: 'app-demo-ngfor-2',
  template: `
    <table>
      <thead><tr><th>ID</th><th>Name</th><th>Age</th><th>Gender</th></tr></thead>
      <tbody>
      <tr *ngFor="let p of persons; index as idx; even as isEven" [ngClass]="{'even':isEven}">
        <td>{{idx+1}}</td>
        <td>{{p.name}}</td>
        <td>{{p.age | number:'1.0-0'}}</td>
        <td>{{p.male ? 'Male' : 'Female'}}</td>
      </tr>
      </tbody>
    </table>
  `,
  styles: [`
    table {width: 16rem;} 
    th {text-align: left;} 
    .even {background-color: rgba(255, 162, 0, 0.5);}
  `]
})
export class DemoNgfor2Component {
  persons: Person[] = [
    {name: "Anna Conda", age: age(), male: false},
    {name: "Inge Vidare", age: age(), male: true},
    {name: "Per Silja", age: age(), male: true},
    {name: "Sham Poo", age: age(), male: true},
    {name: "Sue Purb", age: age(), male: false},
    {name: "Anna Gram", age: age(), male: false},
  ];
}
