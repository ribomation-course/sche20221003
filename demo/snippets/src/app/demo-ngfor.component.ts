import {Component} from "@angular/core";

interface Person {
  name: string;
  age: number;
  male: boolean;
}
const age = () => 20 + Math.floor(40*Math.random());

@Component({
  selector: "app-demo-ngfor",
  template: `
    <table>
      <thead><tr><th>Name</th><th>Age</th><th>Gender</th></tr></thead>
      <tbody>
      <tr *ngFor="let p of persons">
        <td>{{p.name}}</td>
        <td>{{p.age | number:'1.0-0'}}</td>
        <td>{{p.male ? 'Male' : 'Female'}}</td>
      </tr>
      </tbody>
    </table>
  `,
  styles: [`table {width:16rem;} th {text-align: left;}`]
})
export class DemoNgforComponent {
  persons: Person[] = [
    {name: "Anna Conda", age: age(), male: false},
    {name: "Inge Vidare", age: age(), male: true},
    {name: "Per Silja", age: age(), male: true},
    {name: "Sham Poo", age: age(), male: true},
    {name: "Sue Purb", age: age(), male: false},
    {name: "Anna Gram", age: age(), male: false},
  ];
}
