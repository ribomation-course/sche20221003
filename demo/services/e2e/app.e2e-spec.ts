import { ServiceDemoPage } from './app.po';

describe('service-demo App', () => {
  let page: ServiceDemoPage;

  beforeEach(() => {
    page = new ServiceDemoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
