import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  textValue: string = 'simple text';
  numberValue: number = 42;
  boolValue: boolean = true;
  
  textLongValue: string = `This is a much longer text 
  string intended for a textarea and
  filled with just mumbo jumbo!`;
  
  dateValue: Date = new Date();
  onDateChanged(date: any) {
    this.dateValue = new Date(date);
  }
  
  answer: string = 'maybe';
  readonly answers: string[] = ['yes', 'no', 'maybe', 'never'];
  onAnswerChanged(evnt: any) {
    this.answer = evnt.target.value;
  }

  
}


