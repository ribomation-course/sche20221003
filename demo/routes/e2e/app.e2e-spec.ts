import { SimpleRoutesPage } from './app.po';

describe('simple-routes App', () => {
  let page: SimpleRoutesPage;

  beforeEach(() => {
    page = new SimpleRoutesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
