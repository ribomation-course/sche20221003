import {Component, OnInit} from "@angular/core";

@Component({
  template: `
    <div>
      <h3>Admin Stuff</h3>
      <p>
        This is a protected page that requires an
        authenticated (non-anonymous) user.
      </p>
    </div>
  `,
  styles: [`h3 {color: orangered;}`]
})
export class AdminComponent {}
