import {Component, OnInit} from "@angular/core";

interface ProdSpec {
  property: string;
  value: number;
  comment?: string;
}

@Component({
  template: `
    <div>
      <h3>Specification</h3>
      <table>
        <thead><tr><th>Property</th><th>Value</th><th>Comment</th></tr></thead>
        <tbody>
        <tr *ngFor="let s of specs">
          <td>{{s.property | titlecase}}</td>
          <td>{{s.value | number:'1.0-0'}}</td>
          <td>{{s.comment | uppercase}}</td>
        </tr>
        </tbody>
      </table>
    </div>
  `,
})
export class ProductSpecificationComponent  {
  specs: ProdSpec[] = [
    {property:'cool',      value:42, comment:'random'},
    {property:'useful',    value:23, comment:'maybe'},
    {property:'expensive', value:17, comment:'probably'},
  ];
}
