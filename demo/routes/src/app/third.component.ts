import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-third',
  template: `
    <p>This is Third</p>
  `,
})
export class ThirdComponent implements OnInit {
  constructor() { }
  ngOnInit() {}
}
