import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  modalVisible: boolean = false;
  title = 'Just a Silly Message';
  fruits = [
    'apple', 'banana', 'coco nut'
  ];
}
