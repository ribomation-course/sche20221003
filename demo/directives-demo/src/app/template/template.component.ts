import { Component } from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent {
  p1: any = { title: 'first name', field: 'fname', help: 'Your given name' };
  p2: any = { title: 'last name' , field: 'lname', help: 'Your family name' };
  p3: any = { title: 'age'       , field: 'age'  , help: 'Your current age' };
}


