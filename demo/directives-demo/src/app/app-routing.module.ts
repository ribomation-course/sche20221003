import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CssClassComponent } from './css-class/css-class.component';
import { CssPropertyComponent } from './css-property/css-property.component';
import { TemplateComponent } from './template/template.component';
import { UsingIfComponent } from './using-if/using-if.component';
import { UsingSwitchComponent } from './using-switch/using-switch.component';
import { UsingForComponent } from './using-for/using-for.component';

const routes: Route[] = [
  {path: 'css-class', component: CssClassComponent},
  {path: 'css-property', component: CssPropertyComponent},
  {path: 'template', component: TemplateComponent},
  {path: 'using-if', component: UsingIfComponent},
  {path: 'using-switch', component: UsingSwitchComponent},
  {path: 'using-for', component: UsingForComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
