import {Component, OnInit}                             from '@angular/core';
import {Observable}                                    from 'rxjs';
import {FormControl}                                   from '@angular/forms';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {LanguageSearchService}                         from './language-search.service';

@Component({
  selector:    'app-interactive-form-field',
  templateUrl: './interactive-form-field.component.html',
  styleUrls:   ['../app.component.css']
})
export class InteractiveFormFieldComponent implements OnInit {
  phrase = new FormControl('');
  results$: Observable<string[]>;

  constructor(private langSvc: LanguageSearchService) {}

  ngOnInit() {
    this.results$ =
      this.phrase.valueChanges.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(txt => this.langSvc.search(txt))
      );
  }
}


